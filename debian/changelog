lanpower (0.9.6) unstable; urgency=medium

  * Add the reset command to uagx and use offon for cycle

 -- Pierre Neyron <pierre.neyron@free.fr>  Fri, 22 Dec 2023 11:30:49 +0100

lanpower (0.9.5) unstable; urgency=medium

  * Rework oarlanpower: allow changing the soft poweroff command

 -- Pierre Neyron <pierre.neyron@free.fr>  Mon, 06 Nov 2023 17:26:07 +0200

lanpower (0.9.4) unstable; urgency=medium

  * Nothing

 -- Alexandre MERLIN <alexandre.merlin@inria.fr>  Mon, 06 Nov 2023 13:21:41 +0100

lanpower (0.9.3) unstable; urgency=medium

  * [7f96819] Fix wrong cluster configuration used when multiple match

 -- Alexandre MERLIN <alexandre.merlin@inria.fr>  Thu, 02 Nov 2023 14:02:51 +0100

lanpower (0.9.2) unstable; urgency=medium

  * fix trailing space for state action #14738

 -- Laurent Pouilloux <laurent.pouilloux@inria.fr>  Fri, 29 Sep 2023 08:36:47 +0200

lanpower (0.9.1) unstable; urgency=medium

  * Add UAGX support

 -- Pierre Neyron <pierre.neyron@imag.fr>  Fri, 08 Sep 2023 09:55:24 +0200

lanpower (0.9.0) unstable; urgency=medium

  * 8b57e02 [conf] Don't crash if 'nodes' or 'clusters' is missing in conf
  * eb1b5b1 [snmp] Add new SNMP module
  * 455978c [conf] Allow to specify per-node configuration
  * 2ebb82d [lanpower/racadm] Add racadm in bmc requirements
  * cb1fe4a [racadm/lanpower] Add script racadm for lanpower
  * f905702 [deb] update json gem to fix package building
  * 5749570 [rubocop] fix offenses
  * a0f8141 [ci] add rubocop

 -- Baptiste Jonglez <baptiste.jonglez@inria.fr>  Thu, 08 Dec 2022 17:45:49 +0100

lanpower (0.8.3) unstable; urgency=low

  * 875bddc Obfuscate ipmi password from ipmitool command line in logs
  * 0ba8846 Add a log formatter to print lines without decorators

 -- Samir Noir <samir.noir@inria.fr>  Tue, 01 Feb 2022 14:24:03 +0100

lanpower (0.8.2) unstable; urgency=medium

  * Take into account the fact that iDrac >= 3.34.34.34 refuse with an error exit code to on/off a node already on/off

 -- Patrice RINGOT <patrice.ringot@loria.fr>  Fri, 26 Jul 2019 11:47:04 +0200

lanpower (0.8.1) unstable; urgency=medium

  * Initial release 0.8.1

 -- David Loup <david.loup@inria.fr>  Mon, 04 Feb 2019 16:50:06 +0100
