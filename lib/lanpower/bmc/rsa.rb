require "expect"
require "pty"

module Rsa

  def status
    Lanpower::Log.debug("Get RSA status for #{@host}")
    state = rsa_command("power state")
    Lanpower::Log.debug("RSA #{@host} status: #{state}")
    state
  end

  def on
    Lanpower::Log.debug("RSA power on #{@host}")
    rsa_command("power on")
  end

  def off
    Lanpower::Log.debug("RSA power off #{@host}")
    rsa_command("power off")
  end

  def cycle
    Lanpower::Log.debug("RSA power cycle #{@host}")
    rsa_command("power cycle")
  end

  private

  def rsa_command(cmd)
    result = ""
    Lanpower::Log.debug("telnet #{@host}")
    begin
      PTY.spawn("telnet #{@host}") do |read,write,pid|
        write.sync = true
        $expect_verbose = false
        read.expect("username: ", 8) do |response|
          if response
            write.print "#{@user}\r"
          else
            raise "No response from RSA #{@host}"
          end
        end

        read.expect("password: ", 2) do |response|
          write.print "#{@password}\r" if response
        end

        read.expect(/Invalid login!|.*> /, 2) do |response|
          if response
            if response.last =~ /Invalid login!/
              raise "Invalid login on RSA #{@host}"
            else
              write.print "#{cmd}\r" if response
            end
          end
        end

        case cmd
        when "power state"
          read.expect(/Power: (On|Off)/, 5) do |response|
            result = response.nil? ? "error" : response.last.downcase
          end
        end

        read.expect(/.*> /, 2) do |response|
          write.print "exit" if response
        end

        %x[kill #{pid}]
      end
    rescue PTY::ChildExited
      # ignore the exits
    end
    result
  end

end
