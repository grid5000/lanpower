module Racadm

    def status
      Lanpower::Log.debug("Get racadm status for #{@host}")
      power_result = racadm_command("serveraction powerstatus")
      if (!power_result.nil? and power_result.match(/Server power status/))
        state = power_result.split(" ").last.downcase
      else
        state = "error"
      end
      boot_order_result = racadm_command("get BIOS.BiosBootSettings.UefiBootSeq")
      if (!boot_order_result.nil? and boot_order_result.match(/BootSeq/))
        boot_order = boot_order_result.match(/(?<=BootSeq=).*/)[0]
      else
        boot_order = nil
      end
      result = state
      result += " (Boot order: #{boot_order})" if boot_order
      result
    end

    def on
      begin
        Lanpower::Log.debug("racadm power on #{@host}")
        racadm_command("serveraction powerup")
      rescue => e
        if status() == 'on'
          Lanpower::Log.debug("racadm power already on #{@host}")
        else
          raise e
        end
      end
    end

    def off
      begin
        Lanpower::Log.debug("racadm power off #{@host}")
        racadm_command("serveraction powerdown")
      rescue => e
        if status() == 'off'
          Lanpower::Log.debug("racadm power already off #{@host}")
        else
          raise e
        end
      end
    end

    def cycle
      Lanpower::Log.debug("racadm power cycle #{@host}")
      racadm_command("serveraction powercycle")
    end

    def reset
      Lanpower::Log.debug("racadm power reset #{@host}")
      racadm_command("serveraction hardreset")
    end

    private

    def racadm_command(cmd)
      cmd_base = "sshpass -p '#{@password}'"
      cmd_base += " ssh -o 'StrictHostKeyChecking no' #{@user}@#{@host}"
      cmd_base += " racadm" if @bmc == 'racadm'
      cmd_full = "#{cmd_base} #{cmd}"
      if Lanpower::Log.logger.level != Logger::DEBUG
        cmd_full += " 2>/dev/null"
      end
      Lanpower::Log.debug("Command: #{cmd_full.gsub(/sshpass(.+)-p(?:\s*'\w*'\s+)(.*)/, 'sshpass\1-p ***** \2')}")
      result = %x[#{cmd_full}]
      Lanpower::Log.debug("Return value: #{$?}")
      Lanpower::Log.debug("Output value: #{result.strip}")
      raise("Error on BMC with racadm on #{@host} (returned value #{$?})") if $?!=0
      result
    end

  end
