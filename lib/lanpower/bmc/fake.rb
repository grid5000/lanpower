module Fake

  def status
    result = pseudo_random_result(:error => 5, :on => 47, :off => 47)
    raise if result == 'error'
    result
  end

  def on
    result = pseudo_random_result(:error => 5, :ok => 95)
    raise if result == 'error'
    result
  end

  def off
    result = pseudo_random_result(:error => 5, :ok => 95)
    raise if result == 'error'
    result
  end

  def cycle
    result = pseudo_random_result(:error => 10, :ok => 90)
    raise if result == 'error'
    result
  end

  def reset
    result = pseudo_random_result(:error => 10, :ok => 90)
    raise if result == 'error'
    result
  end

  private

  def pseudo_random_result(values)
    array = []
    values.each do |key, value|
      (1..value).each { |_i| array << key.to_s }
    end
    array.sample
  end

end
