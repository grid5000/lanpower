module Uagx
  def status
    Lanpower::Log.debug("Get UAGX status for #{@host}")
    result = uagx_command("getstatus.sh")
    if (!result.nil? and result.match(/^.+:\s*(on|off)$/))
      state = result.split(" ").last
    else
      state = "error"
    end
    Lanpower::Log.debug("UAGX #{@host} status: #{state}")
    state
  end

  def on
    Lanpower::Log.debug("UAGX power on #{@host}")
    uagx_command("toggle.sh on")
  end

  def off
    Lanpower::Log.debug("UAGX power off #{@host}")
    uagx_command("toggle.sh off")
  end

  def cycle
    Lanpower::Log.debug("UAGX cycle #{@host}")
    uagx_command("toggle.sh offon")
  end

  def reset
    Lanpower::Log.debug("UAGX reset #{@host}")
    uagx_command("toggle.sh reset")
  end

  private

  def uagx_command(cmd)
    cmd_full = "sshpass -p '#{@password}' "
    cmd_full += "ssh -F /dev/null -o 'PasswordAuthentication yes' -o 'StrictHostKeyChecking no' #{@user}@#{@host} "
    cmd_full += "sudo #{cmd} #{@host}"
    if Lanpower::Log.logger.level != Logger::DEBUG
      cmd_full += " 2>/dev/null"
    end
    Lanpower::Log.debug("Command: #{cmd_full}")
    result = %x[#{cmd_full}]
    Lanpower::Log.debug("Return value: #{$?}")
    Lanpower::Log.debug("Output value: #{result.strip}")
    raise("Error on BMC with UAGX on #{@host} (returned value #{$?})") if $?!=0
    result
  end

end
