module Ipmi

  def status
    Lanpower::Log.debug("Get IPMI status for #{@host}")
    result = ipmi_command("chassis power status")
    if (!result.nil? and result.match(/Chassis Power is/))
      state = result.split(" ").last
    else
      state = "error"
    end
    Lanpower::Log.debug("IPMI #{@host} status: #{state}")
    state
  end

  def on
    begin
      Lanpower::Log.debug("IPMI power on #{@host}")
      ipmi_command("chassis power on")
    rescue => e
      if status() == 'on'
        Lanpower::Log.debug("IPMI power already on #{@host}")
      else
        raise e
      end
    end
  end

  def off
    begin
      Lanpower::Log.debug("IPMI power off #{@host}")
      ipmi_command("chassis power off")
    rescue => e
      if status() == 'off'
        Lanpower::Log.debug("IPMI power already off #{@host}")
      else
        raise e
      end
    end
  end

  def cycle
    Lanpower::Log.debug("IPMI power cycle #{@host}")
    ipmi_command("chassis power cycle")
  end

  def reset
    Lanpower::Log.debug("IPMI power reset #{@host}")
    ipmi_command("chassis power reset")
  end

  private

  def ipmi_command(cmd)
    cmd_base = "ipmitool -H#{@host}"
    cmd_base += (@user != "NULL") ? " -U#{@user} -P\"#{@password}\"" : " -P\"#{@password}\""
    cmd_base += " -Ilanplus" if @bmc == 'ipmi2'
    cmd_full = "#{cmd_base} #{cmd}"
    if Lanpower::Log.logger.level != Logger::DEBUG
      cmd_full += " 2>/dev/null"
    end
    Lanpower::Log.debug("Command: #{cmd_full.gsub(/ipmitool(.+)-P(?:\s*".*"\s+)(.*)/, 'ipmitool\1-P***** \2')}")
    result = %x[#{cmd_full}]
    Lanpower::Log.debug("Return value: #{$?}")
    Lanpower::Log.debug("Output value: #{result.strip}")
    raise("Error on BMC with ipmitool on #{@host} (returned value #{$?})") if $?!=0
    result
  end

end
