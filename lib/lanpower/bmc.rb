require "lanpower/bmc/ipmi"
require "lanpower/bmc/racadm"
require "lanpower/bmc/rsa"
require "lanpower/bmc/fake"
require "lanpower/bmc/uagx"

class Lanpower
  class Bmc

    attr_accessor :host, :bmc, :user
    attr_writer :password

    def initialize(options = {})
      @options = options

      %w[host bmc user password].each do |key|
        raise ArgumentError, "BMC instance need :#{key}" if @options[:"#{key}"].nil?
      end

      @host       = @options[:host]
      @bmc        = @options[:bmc]
      @user       = @options[:user]
      @password   = @options[:password]

      case @bmc
      when "ipmi"
        self.extend(Ipmi)
      when "ipmi2"
        self.extend(Ipmi)
      when "rsa"
        self.extend(Rsa)
      when "racadm"
        self.extend(Racadm)
      when "uagx"
        self.extend(Uagx)
      when "fake"
        self.extend(Fake)
      end
    end
  end
end

