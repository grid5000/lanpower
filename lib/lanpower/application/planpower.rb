require "lanpower"
require "lanpower/application"
require "lanpower/config"
require "lanpower/bmc"
require "lanpower/log"
require 'timeout'
require 'syslog'

class Lanpower::Application::Planpower < Lanpower::Application

  option :help,
    :short        => "-h",
    :long         => "--help",
    :description  => "Show this message",
    :on           => :tail,
    :boolean      => true,
    :show_options => true,
    :exit         => 0

  option :config_file,
    :short        => "-c CONFIG",
    :long         => "--config-file CONFIG",
    :description  => "Set configuration file (default: #{Lanpower::CONFIG_FILE})",
    :default      => Lanpower::CONFIG_FILE,
    :proc         => nil

  option :status,
    :long         => "--status",
    :description  => "Power status",
    :boolean      => true,
    :default      => true,
    :proc         => nil

  option :on,
    :long         => "--on",
    :description  => "Power on",
    :boolean      => true,
    :default      => false,
    :proc         => nil

  option :off,
    :long         => "--off",
    :description  => "Power off",
    :boolean      => true,
    :default      => false,
    :proc         => nil

  option :cycle,
    :long         => "--cycle",
    :description  => "Power cycle",
    :boolean      => true,
    :default      => false,
    :proc         => nil

  option :safety_cycle,
    :long         => "--safety-cycle",
    :description  => "Safety power cycle",
    :boolean      => true,
    :default      => false,
    :proc         => nil

  option :sleep,
    :short        => "-s SEC",
    :long         => "--sleep SEC",
    :description  => "Sleep SEC seconds between power on/off on a safety cycle (default: 5)",
    :default      => 5,
    :proc         => nil

  option :retry,
    :long         => "--retry SEC",
    :description  => "Retry on error after delay in seconds (default: 10)",
    :default      => 10,
    :proc         => nil

  option :hostname,
    :short        => "-H HOSTNAME",
    :long         => "--hostname HOSTNAME",
    :description  => "The hostname of the BMC (disable STDIN nodes list input)",
    :proc         => nil

  option :version,
    :short        => "-v",
    :long         => "--version",
    :description  => "Show version",
    :boolean      => true,
    :proc         => lambda {|_v| puts "lanpower: #{Lanpower::VERSION}"},
    :exit         => 0

  option :disable_log_decorators,
    :long         => "--disable-log-decorators",
    :description  => "Do not decorate log outputs, print message followed by datetime",
    :boolean      => true,
    :default      => false

  def initialize
    super
    @nodes = Array.new
    Syslog.open('lanpower')
  end

  def run
    # use parse_options from Mixlib
    parse_options

    Lanpower::Log.init() if config[:debug]
    Lanpower::Log.level(config[:debug] ? :debug : :info)
    Lanpower::Log.formatter = Lanpower::Log::NoDecoratorFormatter if config[:disable_log_decorators]

    conf        = Lanpower::Config.new(config[:config_file])
    clusters    = conf.value["clusters"].keys
    nodes       = Array.new
    input       = Array.new

    if config[:hostname].nil?
      STDIN.each do |line|
        input << line.chomp.split(".").first
      end
    else
      input << config[:hostname].split(".").first
    end

    config[:status] = false if (config[:on] or config[:off] or config[:cycle] or config[:safety_cycle])

    input.each do |h|
      cluster     = clusters.select { |c| !h.match(c).nil? }.first
      if !cluster.nil?
        node       = Lanpower::Bmc.new(
          :host       => h + conf.value["clusters"][cluster]["suffix"],
          :bmc        => conf.value["clusters"][cluster]["bmc"],
          :user       => conf.value["clusters"][cluster]["user"],
          :password   => conf.value["clusters"][cluster]["password"]
        )
      else
        raise ArgumentError, "Node #{h} doesn't match any clusters in configuration file"
      end
      nodes << node
    end

    cmd = 'status'
    %w{ safety_cycle cycle off on status}.each do |option|
      cmd = option if config[option.to_sym]
    end

    workq = Queue.new
    nodes.each { |host| workq << host }
    result= {}
    cmd_retry= {}

    workers = (0..20).map do
      Thread.new do
        begin
          while (node = workq.pop(true))
            begin
              Timeout::timeout(10) do
                result[node.host] = node.send(cmd)
                cmd_retry[node.host] = false
              end
            rescue
              result[node.host] = "error"
              if not cmd_retry[node.host]
                cmd_retry[node.host] = true
                Syslog.info("retry #{cmd} command on host #{node.host} into #{config[:retry]} seconds")
                sleep(config[:retry].to_i)
                workq << node
              end
            end
          end
        rescue ThreadError
          # Do nothing
        end
      end
    end

    workers.map(&:join)

    result.each do |host, status|
      puts host + ": " + status
    end

    if not result.select { |_k,v| v == "error" }.empty?
      Syslog.info("Some nodes are into error")
      exit 1
    end

  end

end
