require "lanpower"
require "lanpower/application"
require "lanpower/config"
require "lanpower/bmc"
require "lanpower/log"
require "lanpower/task"
require 'timeout'
require 'syslog'
require 'concurrent'

class Lanpower::Application::OARlanpower < Lanpower::Application

  option :help,
    :short        => "-h",
    :long         => "--help",
    :description  => "Show this message",
    :on           => :tail,
    :boolean      => true,
    :show_options => true,
    :exit         => 0

  option :config_file,
    :short        => "-c CONFIG",
    :long         => "--config-file CONFIG",
    :description  => "Set configuration file (default: #{Lanpower::CONFIG_FILE})",
    :default      => Lanpower::CONFIG_FILE,
    :proc         => nil

  option :turn,
    :long         => "--turn STATUS",
    :description  => "Power on/off",
    :required     => true,
    :in           => ['on', 'off'],
    :proc         => nil

  option :debug,
    :short        => "-d",
    :long         => "--debug",
    :description  => "Active debug mode",
    :boolean      => true,
    :proc         => nil

  option :version,
    :short        => "-v",
    :long         => "--version",
    :description  => "Show version",
    :boolean      => true,
    :proc         => lambda {|_v| puts "lanpower: #{Lanpower::VERSION}"},
    :exit         => 0

  option :disable_log_decorators,
    :long         => "--disable-log-decorators",
    :description  => "Do not decorate log outputs, print message followed by datetime",
    :boolean      => true,
    :default      => false

  def initialize
    super
    @nodes = {}
    Syslog.open('lanpower')
  end

  def run
    # use parse_options from Mixlib
    parse_options

    Lanpower::Log.init if config[:debug]
    Lanpower::Log.level(config[:debug] ? :debug : :info)
    Lanpower::Log.formatter = Lanpower::Log::NoDecoratorFormatter if config[:disable_log_decorators]

    conf = Lanpower::Config.new(config[:config_file])

    input_nodes = []
    $stdin.each do |line|
      input_nodes << line.chomp.split('.').first
    end

    input_nodes.each do |h|
      begin
        cluster_name = h.split('-').first
        clusterConf = conf.value['clusters'][cluster_name]
        node_bmc = Lanpower::Bmc.new(
          :host       => h + clusterConf['suffix'],
          :bmc        => clusterConf['bmc'],
          :user       => clusterConf['user'],
          :password   => clusterConf['password']
        )
        @nodes[h] = {}
        @nodes[h][:bmc] = node_bmc
        @nodes[h][:powerStatusRetry] = clusterConf['oarPowerStatusRetry'] || 5
        @nodes[h][:powerStatusDelay] = clusterConf['oarPowerStatusDelay'] || 10
        @nodes[h][:softPowerOffDelay] = clusterConf['oarSoftPowerOffDelay'] || 30
        @nodes[h][:ignoreError] = clusterConf['oarIgnoreError'] || false
        @nodes[h][:oarNodeConnectCmd] = clusterConf['oarNodeConnectCmd'] || 'ssh -p 6667 -i /var/lib/oar/.ssh/id_rsa -o ServerAliveInterval=1 -o ServerAliveCountMax=1 -l oar'
        @nodes[h][:oarSoftPowerOffCmd] = clusterConf['oarSoftPowerOffCmd'] || 'oardodo nohup /sbin/poweroff &>/dev/null'
      rescue
        raise ArgumentError, "Error in the cluster configuration of node #{h} (#{config[:config_file]})"
      end
    end

    case config[:turn]
    when 'on'
      results = wakeup
    when 'off'
      results = poweroff
    else
      puts '--turn must be on|off'
      exit 1
    end

    Lanpower::Task.synchronize

    results.each do |host, status|
      puts host + ': ' + status
    end

    if not results.select { |k,v| v == 'error' and @nodes[k][:ignoreError] == false }.empty?
      Syslog.info('Some nodes are in error')
      exit 1
    end

  end

  private

  def softPowerOff
    @nodes.each do |n, c|
      softpoweroff_task = Lanpower::Task.new :desc => "Soft power off node #{n}"
      softpoweroff_task.define do
        result = %x[#{c[:oarNodeConnectCmd]} #{n} "#{c[:oarSoftPowerOffCmd]}"]
        puts result if config[:debug]
      end
      softpoweroff_task.schedule
    end
  end

  def poweroff
    softPowerOff
    results = {}
    @nodes.each do |n, c|
      powerstatus_task = Lanpower::Task.new :desc => "Power status node #{n}"
      powerstatus_task.define do
        begin
          results[n] = c[:bmc].send('status')
          if results[n] == 'on'
            if powerstatus_task.retry_count >= (powerstatus_task.times || 100)
              poweroff_task = Lanpower::Task.new :desc => "Power off node #{n}"
              poweroff_task.define do
                begin
                  results[n] = c[:bmc].send('off')
                  powerstatus_task.retryWithDelay(6, c[:powerStatusDelay])
                rescue => powerOffException
                  puts "IPMI power off on node #{n} failed" if config[:debug]
                  puts powerOffException if config[:debug]
                  poweroff_task.retryWithDelay(3, 10)
                  results[n] = 'error'
                end
              end
              poweroff_task.schedule
            else
              powerstatus_task.retryWithDelay(c[:powerStatusRetry], c[:powerStatusDelay])
            end
          end
          results[n]
        rescue => powerStatusException
          puts "IPMI power status on node #{n} failed" if config[:debug]
          puts powerStatusException if config[:debug]
          powerstatus_task.retryWithDelay(c[:powerStatusRetry], c[:powerStatusDelay])
          results[n] = 'error'
        end
      end
      powerstatus_task.scheduleWithDelay(c[:softPowerOffDelay])
    end
    results
  end

  def wakeup
    results = {}
    @nodes.each do |n, c|
      wakeup_task = Lanpower::Task.new :desc => "Power on node #{n}"
      wakeup_task.define do
        begin
          results[n] = c[:bmc].send('on')
          powerstatus_task = Lanpower::Task.new :desc => "Power status node #{n}"
          powerstatus_task.define do
            begin
              results[n] = c[:bmc].send('status')
              if results[n] == 'off'
                wakeup_task.retryWithDelay(2,5)
              end
              results[n]
            rescue => powerStatusException
              puts "IPMI power status on node #{n} failed" if config[:debug]
              puts powerStatusException if config[:debug]
              powerstatus_task.retryWithDelay(5, 10)
              results[n] = 'error'
            end
          end
          powerstatus_task.scheduleWithDelay(10)
        rescue => powerOnException
          puts "IMPI power on on node #{n} failed" if config[:debug]
          puts powerOnException if config[:debug]
          wakeup_task.retryWithDelay(3, 10)
          results[n] = 'error'
        end
        results[n]
      end
      wakeup_task.schedule
    end
    results
  end

end
