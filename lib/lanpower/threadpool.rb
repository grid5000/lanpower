class Lanpower
  class ThreadPool
    def initialize(max_size)
      @pool = []
      @max_size = max_size
      @pool_mutex = Mutex.new
      @pool_cv = ConditionVariable.new
    end

    def dispatch(*args)
      Thread.new do
        @pool_mutex.synchronize do
          while @pool.size >= @max_size
            @pool_cv.wait(@pool_mutex)
          end
        end
        @pool << Thread.current
        begin
          yield(*args)
        rescue => e
          exception(self, e, *args)
        ensure
          @pool_mutex.synchronize do
            @pool.delete(Thread.current)
            @pool_cv.signal
          end
        end
      end
    end

    def shutdown
      @pool_mutex.synchronize { @pool_cv.wait(@pool_mutex) until @pool.empty? }
    end

    def exception(thread, exception, *_original_args)
      puts "Exception in thread #{thread}: #{exception.message} - #{exception.backtrace}"
    end
  end
end
