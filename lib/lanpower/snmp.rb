class Lanpower
  class Snmp

    attr_accessor :host, :snmp_device, :snmp_version, :community, :oid, :snmp_on, :snmp_off

    def initialize(options = {})
      @options = options

      %w[host snmp_device snmp_version community oid snmp_on snmp_off].each do |key|
        raise ArgumentError, "SNMP instance need :#{key}" if @options[:"#{key}"].nil?
      end

      @host         = @options[:host]
      @snmp_device  = @options[:snmp_device]
      @snmp_version = @options[:snmp_version]
      @community    = @options[:community]
      @oid          = @options[:oid]
      @snmp_on      = @options[:snmp_on]
      @snmp_off     = @options[:snmp_off]
    end

    def status
      Lanpower::Log.debug("Get SNMP power status for #{@host} using #{@snmp_device}")
      result = snmp_get_power()
      if !result.nil?
        case result
        when /#{@snmp_on}/
          state = "on"
        when /#{@snmp_off}/
          state = "off"
        else
          state = "error"
        end
      else
        state = "error"
      end
      Lanpower::Log.debug("SNMP #{@host} status: #{state}")
      state
    end

  def on
    begin
      Lanpower::Log.debug("SNMP power on for #{@host} using #{@snmp_device}")
      snmp_set_power(@snmp_on)
    rescue => e
      if status() == 'on'
        Lanpower::Log.debug("SNMP power already on for #{@host}")
      else
        raise e
      end
    end
  end

  def off
    begin
      Lanpower::Log.debug("SNMP power off for #{@host} using #{@snmp_device}")
      snmp_set_power(@snmp_off)
    rescue => e
      if status() == 'off'
        Lanpower::Log.debug("SNMP power already off for #{@host}")
      else
        raise e
      end
    end
  end

  def cycle
    Lanpower::Log.debug("SNMP power cycle for #{@host} using #{@snmp_device}")
    off()
    sleep 1
    on()
  end

  def reset
    Lanpower::Log.debug("SNMP power reset for #{@host} using #{@snmp_device}")
    off()
    sleep 1
    on()
  end

  private

  def snmp_get_power()
    cmd = "snmpget -Ov -Oq -v#{@snmp_version} -c \"#{@community}\" #{@snmp_device} #{@oid}"
    if Lanpower::Log.logger.level != Logger::DEBUG
      cmd += " 2>/dev/null"
    end
    Lanpower::Log.debug("Command: #{cmd.gsub(/snmpget(.+)-c (?:\s*"[a-zA-Z0-9]*"\s+)(.*)/, 'snmpget\1-c ***** \2')}")
    result = %x[#{cmd}]
    Lanpower::Log.debug("Return value: #{$?}")
    Lanpower::Log.debug("Output value: #{result.strip}")
    raise("Error on SNMP with snmpget on #{@snmp_device} for #{@host} (returned value #{$?})") if $?!=0
    result
  end

  def snmp_set_power(value)
    cmd = "snmpset -v#{@snmp_version} -c \"#{@community}\" #{@snmp_device} #{@oid} i #{value}"
    if Lanpower::Log.logger.level != Logger::DEBUG
      cmd += " 2>/dev/null"
    end
    Lanpower::Log.debug("Command: #{cmd.gsub(/snmpset(.+)-c (?:\s*"[a-zA-Z0-9]*"\s+)(.*)/, 'snmpset\1-c ***** \2')}")
    result = %x[#{cmd}]
    Lanpower::Log.debug("Return value: #{$?}")
    Lanpower::Log.debug("Output value: #{result.strip}")
    raise("Error on SNMP with snmpset on #{@snmp_device} for #{@host} (returned value #{$?})") if $?!=0
    result
  end

  end
end
