require "yaml"

class Lanpower
  class Config

    @conf = nil

    def initialize(file)
      if @conf.nil?
        begin
          if File.exist?(file)
            @conf = YAML.load(File.read(file))
          else
            puts "Configuration file is needed: #{file}"
            exit 0
          end
        rescue => error
          puts "Error : #{error.message}"
        end
      end
    end

    def value
      @conf
    end

  end
end
